package at.implo.resource_core.registry;

import at.implo.resource_core.container.CoreExtractorContainer;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.extensions.IForgeContainerType;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class ContainerRegistry {

    private static final DeferredRegister<ContainerType<?>> CONTAINERS = DeferredRegister.create(ForgeRegistries.CONTAINERS, "resource_core");
    public static final RegistryObject<ContainerType<CoreExtractorContainer>> CORE_EXTRACTOR_CONTAIENR = createCoreExtractorContainer();

    public static void register(IEventBus eventBus) {
        CONTAINERS.register(eventBus);
    }

    public static RegistryObject<ContainerType<CoreExtractorContainer>> createCoreExtractorContainer() {
        return CONTAINERS.register("core_extractor_container", () -> {
            return IForgeContainerType.create((id, inv, data) -> {
                BlockPos position = data.readBlockPos();
                World world = inv.player.getCommandSenderWorld();
                return new CoreExtractorContainer(id, world, position, inv, inv.player);
            });
        });
    }
}
