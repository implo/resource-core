package at.implo.resource_core.registry;

import at.implo.resource_core.item.CoreExtractor;
import at.implo.resource_core.item.IronCore;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class ItemRegistry {

    private static final DeferredRegister<Item> ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS, "resource_core");
    public static Item IRON_CORE = new IronCore();
    public static BlockItem CORE_EXTRACTOR = new CoreExtractor();

    public static void register(IEventBus eventBus) {
        ITEMS.register(eventBus);
        registerItem("iron_core",IRON_CORE);
        registerItem("core_extractor", CORE_EXTRACTOR);
    }

    public static void registerItem(String name, Item item) {
        System.out.println("registering iron core");
        ITEMS.register(name, () -> item);
    }

}
