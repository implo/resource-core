package at.implo.resource_core.registry;

import at.implo.resource_core.block.CoreExtractor;
import net.minecraft.block.Block;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class BlockRegistry {

    private static final DeferredRegister<Block> BLOCKS = DeferredRegister.create(ForgeRegistries.BLOCKS, "resource_core");
    public static Block CORE_EXTRACTOR = new CoreExtractor();

    public static void register(IEventBus eventBus) {
        BLOCKS.register(eventBus);
        registerBlock("core_extractor", CORE_EXTRACTOR);
    }

    public static void registerBlock(String name, Block block) {
        System.out.println("registering iron core");
        BLOCKS.register(name, () -> block);
    }


}
