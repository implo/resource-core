package at.implo.resource_core.registry;

import at.implo.resource_core.tileentity.CoreExtractorTileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class TileEntityRegistry {


    public static DeferredRegister<TileEntityType<?>> TILE_ENTITIES 
        = DeferredRegister.create(ForgeRegistries.TILE_ENTITIES, "resource_core");

    public static RegistryObject<TileEntityType<CoreExtractorTileEntity>> CORE_EXTRACTOR_TILE_ENTITY_TYPE = TILE_ENTITIES
        .register("core_extractor_tile_entity", () -> 
                TileEntityType.Builder.of(CoreExtractorTileEntity::new,
                BlockRegistry.CORE_EXTRACTOR
        ).build(null));

    // public static CoreExtractorTileEntity CORE_EXTRACTOR_TILE_ENTITY = new CoreExtractorTileEntity();

    public static void register(IEventBus eventBus) {
        TILE_ENTITIES.register(eventBus);
    }
}
