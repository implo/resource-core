package at.implo.resource_core.registry;

import at.implo.resource_core.tabs.ResourceCoreTab;
import net.minecraft.item.ItemGroup;

public class ItemTabRegistry {

    public static ItemGroup RESOURCE_CORE_TAB = new ResourceCoreTab();
}
