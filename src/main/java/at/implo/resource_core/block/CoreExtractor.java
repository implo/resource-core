package at.implo.resource_core.block;

import java.util.Random;

import at.implo.resource_core.container.CoreExtractorContainer;
import at.implo.resource_core.registry.TileEntityRegistry;
import at.implo.resource_core.tileentity.CoreExtractorTileEntity;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.material.Material;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.tileentity.FurnaceTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ActionResultType;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.fml.network.NetworkHooks;

public class CoreExtractor extends Block {

    static final Properties properties = Properties.of(Material.METAL);

    public CoreExtractor() {
        super(properties);
    }

    @Override
    public boolean hasTileEntity(BlockState state) {
        return true;
    }

    @Override
    public TileEntity createTileEntity(BlockState state, IBlockReader world) {
        return TileEntityRegistry.CORE_EXTRACTOR_TILE_ENTITY_TYPE.get().create();
    }

    @Override
    public ActionResultType use(BlockState blockState, World world, BlockPos position,
            PlayerEntity player, Hand hand, BlockRayTraceResult rayTrace) {
        if (!world.isClientSide()) {
            TileEntity tileEntity = world.getBlockEntity(position);

            if (tileEntity instanceof CoreExtractorTileEntity) {
                INamedContainerProvider containerProvider = createContainerProvider(world, position);

                NetworkHooks.openGui(((ServerPlayerEntity) player), containerProvider, tileEntity.getBlockPos());
            } else {
                throw new IllegalStateException("Container Provider Missing");
            }

        }

        return ActionResultType.SUCCESS;
    }

    private INamedContainerProvider createContainerProvider(World world, BlockPos position) {
        return new INamedContainerProvider() {
            @Override
            public ITextComponent getDisplayName() {
                return new TranslationTextComponent("screen.resource_core.core_extractor");
            }

            @Override
            public Container createMenu(int id, PlayerInventory inventory,
                    PlayerEntity player) {
                return new CoreExtractorContainer(id, world, position, inventory, player);
            }

        };
    }
}
