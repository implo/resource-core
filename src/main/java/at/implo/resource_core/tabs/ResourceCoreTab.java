package at.implo.resource_core.tabs;

import at.implo.resource_core.registry.ItemRegistry;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;

public class ResourceCoreTab extends ItemGroup {

    public ResourceCoreTab() {
        super("resource_core_tab");
    }

    @Override
    public ItemStack makeIcon() {
        return new ItemStack(ItemRegistry.IRON_CORE);
    }

}
