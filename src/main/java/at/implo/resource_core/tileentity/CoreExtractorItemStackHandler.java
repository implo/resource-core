package at.implo.resource_core.tileentity;

import java.util.ArrayList;
import java.util.List;

import at.implo.resource_core.registry.ItemRegistry;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraftforge.items.ItemStackHandler;

public class CoreExtractorItemStackHandler extends ItemStackHandler {

    public List<OnCoreChangeEvent> subscriber = new ArrayList<>();

    public CoreExtractorItemStackHandler() {
        super(2);
    }

    @Override
    public boolean isItemValid(int slot, ItemStack stack) {
        switch (slot) {
            case 0:
                return stack.getItem() == ItemRegistry.IRON_CORE;
            case 1:
                return stack.getItem() == Items.IRON_INGOT;
            default:
                return false;
        }
    }

    @Override
    public int getSlotLimit(int slot) {
        return 1;
    }

    @Override
    public ItemStack insertItem(int slot, ItemStack stack, boolean simulate) {
        if (!isItemValid(slot, stack)) {
            return stack;
        }

        return super.insertItem(slot, stack, simulate);
    }

    public void onCoreSlotChange(OnCoreChangeEvent onCoreChange) {
        subscriber.add(onCoreChange);
    }

    private void notifyCoreSlotChangeSubscriber() {
        for (OnCoreChangeEvent subscriber : subscriber) {
            subscriber.event();
        }
    }

    @Override
    public ItemStack extractItem(int slot, int amount, boolean simulate) {
        if (slot == 0) {
            notifyCoreSlotChangeSubscriber();
        }

        return super.extractItem(slot, amount, simulate);
    }
}
