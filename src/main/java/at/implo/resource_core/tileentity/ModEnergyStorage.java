package at.implo.resource_core.tileentity;

import net.minecraftforge.energy.EnergyStorage;

public class ModEnergyStorage extends EnergyStorage {

    public ModEnergyStorage(int capacity, int transfer, int energy) {
        super(capacity, transfer, transfer, energy);
    }

    public int setEnergyStored(int energy) {
        this.energy = energy;
        return this.energy;
    }

}
