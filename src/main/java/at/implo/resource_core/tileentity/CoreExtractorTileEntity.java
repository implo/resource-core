package at.implo.resource_core.tileentity;

import at.implo.resource_core.registry.ItemRegistry;
import at.implo.resource_core.registry.TileEntityRegistry;
import net.minecraft.block.BlockState;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SUpdateTileEntityPacket;
import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.Constants;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.IEnergyStorage;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.IItemHandler;

public class CoreExtractorTileEntity extends TileEntity implements ITickableTileEntity {

    private final CoreExtractorItemStackHandler inventory = new CoreExtractorItemStackHandler();
    private LazyOptional<IItemHandler> inventoryHandler = LazyOptional.empty();
    public final ModEnergyStorage energy = new ModEnergyStorage(500_000, 10_000, 0);
    private LazyOptional<IEnergyStorage> energyHandler = LazyOptional.empty();
    private final double progression_time = 0.1;
    // private static final Logger LOGGER = LogManager.getLogger();

    public double progress = 0;

    public CoreExtractorTileEntity(TileEntityType<?> type_in) {
        super(type_in);
    }

    public CoreExtractorTileEntity() {
        super(TileEntityRegistry.CORE_EXTRACTOR_TILE_ENTITY_TYPE.get());
        this.inventory.onCoreSlotChange(() -> {
            this.progress = 0;
        });
    }

    @Override
    public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side) {
        if (cap == CapabilityEnergy.ENERGY) {
            return energyHandler.cast();
        }

        if (cap == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY) {
            return inventoryHandler.cast();
        }

        return super.getCapability(cap, side);
    }

    @Override
    public CompoundNBT save(CompoundNBT compound) {
        compound.put("inv", inventory.serializeNBT());
        compound.putDouble("progress", this.progress);
        compound.putInt("energy", this.energy.getEnergyStored());
        return super.save(compound);
    }

    @Override
    public void load(BlockState state, CompoundNBT nbt) {
        inventory.deserializeNBT(nbt.getCompound("inv"));
        this.progress = nbt.getDouble("progress");
        this.energy.setEnergyStored(nbt.getInt("energy"));
        super.load(state, nbt);
    }

    @Override
    public SUpdateTileEntityPacket getUpdatePacket() {
        return new SUpdateTileEntityPacket(getBlockPos(), 1, getUpdateTag());
    }

    @Override
    public void onDataPacket(NetworkManager net, SUpdateTileEntityPacket pkt) {
        handleUpdateTag(level.getBlockState(pkt.getPos()), pkt.getTag());
    }

    @Override
    public CompoundNBT getUpdateTag() {
        CompoundNBT nbt = super.getUpdateTag();
        nbt.putInt("energy", this.energy.getEnergyStored());
        nbt.putDouble("progress", this.progress);
        return nbt;
    }

    @Override
    public void handleUpdateTag(BlockState state, CompoundNBT tag) {
        this.energy.setEnergyStored(tag.getInt("energy"));
        this.progress = tag.getDouble("progress");
    }

    @Override
    public void onLoad() {
        this.energyHandler = LazyOptional.of(() -> energy);
        this.inventoryHandler = LazyOptional.of(() -> inventory);
        super.onLoad();
    }

    @Override
    public void tick() {
        if (this.hasCoreInjected() && energy.getEnergyStored() >= 10 && !level.isClientSide()) {
            progress();
        }

        sync();
    }

    public void sync() {
        this.level.sendBlockUpdated(getBlockPos(), getBlockState(), getBlockState(), Constants.BlockFlags.BLOCK_UPDATE);
    }

    private boolean hasCoreInjected() {
        return this.inventory.getStackInSlot(0).getItem() == ItemRegistry.IRON_CORE;
    }

    private void progress() {
        if (this.progress >= 100) {
            addOneResource();
        }

        this.energy.extractEnergy(500, false);
        this.progress += progression_time;
    }

    private void addOneResource() {
        ItemStack current = this.inventory.getStackInSlot(1);
        if (current.getCount() == 0) {
            this.inventory.insertItem(1, new ItemStack(Items.IRON_INGOT, 1), false);
        }
        current.setCount(current.getCount() + 1);
        this.inventory.insertItem(1, current, false);
        this.progress = 0;
    }

}
