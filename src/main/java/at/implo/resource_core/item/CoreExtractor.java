package at.implo.resource_core.item;

import at.implo.resource_core.registry.BlockRegistry;
import at.implo.resource_core.registry.ItemTabRegistry;
import net.minecraft.item.BlockItem;

public class CoreExtractor extends BlockItem {

    static final Properties properties = new Properties()
        .tab(ItemTabRegistry.RESOURCE_CORE_TAB);

    public CoreExtractor() {
        super(BlockRegistry.CORE_EXTRACTOR, properties);
    }

}
