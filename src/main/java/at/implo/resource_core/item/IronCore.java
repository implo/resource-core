package at.implo.resource_core.item;

import at.implo.resource_core.registry.ItemTabRegistry;
import net.minecraft.item.Item;

public class IronCore extends Item {

    static Properties properties = new Properties()
            .tab(ItemTabRegistry.RESOURCE_CORE_TAB);

    public IronCore() {
        super(IronCore.properties);
    }

}
