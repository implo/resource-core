package at.implo.resource_core.screen;

import com.mojang.blaze3d.matrix.MatrixStack;

import at.implo.resource_core.container.CoreExtractorContainer;
import at.implo.resource_core.tileentity.CoreExtractorTileEntity;
import net.minecraft.client.gui.screen.inventory.ContainerScreen;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;

public class CoreExtractorScreen extends ContainerScreen<CoreExtractorContainer> {

    private static final ResourceLocation GUI = new ResourceLocation(
            "resource_core", "textures/gui/core_extractor.png");

    private final CoreExtractorTileEntity tileEntity;

    public CoreExtractorScreen(CoreExtractorContainer container, PlayerInventory inventory,
            ITextComponent title) {
        super(container, inventory, title);
        this.tileEntity = (CoreExtractorTileEntity) container.tileEntity;
    }

    @Override
    protected void renderBg(MatrixStack matrixstack, float p_230450_2_, int p_230450_3_, int p_230450_4_) {
        this.minecraft.getTextureManager().bind(GUI);
        this.blit(matrixstack, getGuiLeft(), getGuiTop(), 0, 0, this.getXSize(), this.getYSize());
    }

    @Override
    public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
        renderBackground(matrixStack);
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        this.renderTooltip(matrixStack, mouseX, mouseY);

        int left = this.getGuiLeft();
        int top = this.getGuiTop();
        String progressString = String.format("%.0f%%", this.tileEntity.progress);
        String energyString = String.format("%.1f/%.1f kFE",
                (double) this.tileEntity.energy.getEnergyStored() / 1000,
                (double) this.tileEntity.energy.getMaxEnergyStored() / 1000
        );

        this.font.draw(
                matrixStack,
                ITextComponent.nullToEmpty(progressString),
                left + 80 , top + 31, 0
        );

        this.font.draw(
                matrixStack,
                ITextComponent.nullToEmpty(energyString),
                left + 80 , top + 50, 0
        );
    }


}
